package com.amplifyframework.datastore.generated.model;

import com.amplifyframework.core.model.temporal.Temporal;

import java.util.List;
import java.util.UUID;
import java.util.Objects;

import androidx.core.util.ObjectsCompat;

import com.amplifyframework.core.model.AuthStrategy;
import com.amplifyframework.core.model.Model;
import com.amplifyframework.core.model.ModelOperation;
import com.amplifyframework.core.model.annotations.AuthRule;
import com.amplifyframework.core.model.annotations.Index;
import com.amplifyframework.core.model.annotations.ModelConfig;
import com.amplifyframework.core.model.annotations.ModelField;
import com.amplifyframework.core.model.query.predicate.QueryField;

import static com.amplifyframework.core.model.query.predicate.QueryField.field;

/** This is an auto generated class representing the Attendance type in your schema. */
@SuppressWarnings("all")
@ModelConfig(pluralName = "Attendances", authRules = {
  @AuthRule(allow = AuthStrategy.PRIVATE, operations = { ModelOperation.CREATE, ModelOperation.UPDATE, ModelOperation.DELETE, ModelOperation.READ })
})
@Index(name = "byEvent", fields = {"eventID"})
@Index(name = "byUser", fields = {"userID"})
public final class Attendance implements Model {
  public static final QueryField ID = field("Attendance", "id");
  public static final QueryField EVENT_ID = field("Attendance", "eventID");
  public static final QueryField USER_ID = field("Attendance", "userID");
  public static final QueryField ATTENDED = field("Attendance", "attended");
  public static final QueryField ATTENDED_DATE_TIME = field("Attendance", "attendedDateTime");
  public static final QueryField MANDATORY_EVENT = field("Attendance", "mandatoryEvent");
  private final @ModelField(targetType="ID", isRequired = true) String id;
  private final @ModelField(targetType="ID") String eventID;
  private final @ModelField(targetType="ID") String userID;
  private final @ModelField(targetType="Boolean") Boolean attended;
  private final @ModelField(targetType="AWSDateTime") Temporal.DateTime attendedDateTime;
  private final @ModelField(targetType="Boolean") Boolean mandatoryEvent;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime createdAt;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime updatedAt;
  public String getId() {
      return id;
  }
  
  public String getEventId() {
      return eventID;
  }
  
  public String getUserId() {
      return userID;
  }
  
  public Boolean getAttended() {
      return attended;
  }
  
  public Temporal.DateTime getAttendedDateTime() {
      return attendedDateTime;
  }
  
  public Boolean getMandatoryEvent() {
      return mandatoryEvent;
  }
  
  public Temporal.DateTime getCreatedAt() {
      return createdAt;
  }
  
  public Temporal.DateTime getUpdatedAt() {
      return updatedAt;
  }
  
  private Attendance(String id, String eventID, String userID, Boolean attended, Temporal.DateTime attendedDateTime, Boolean mandatoryEvent) {
    this.id = id;
    this.eventID = eventID;
    this.userID = userID;
    this.attended = attended;
    this.attendedDateTime = attendedDateTime;
    this.mandatoryEvent = mandatoryEvent;
  }
  
  @Override
   public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      } else if(obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
      Attendance attendance = (Attendance) obj;
      return ObjectsCompat.equals(getId(), attendance.getId()) &&
              ObjectsCompat.equals(getEventId(), attendance.getEventId()) &&
              ObjectsCompat.equals(getUserId(), attendance.getUserId()) &&
              ObjectsCompat.equals(getAttended(), attendance.getAttended()) &&
              ObjectsCompat.equals(getAttendedDateTime(), attendance.getAttendedDateTime()) &&
              ObjectsCompat.equals(getMandatoryEvent(), attendance.getMandatoryEvent()) &&
              ObjectsCompat.equals(getCreatedAt(), attendance.getCreatedAt()) &&
              ObjectsCompat.equals(getUpdatedAt(), attendance.getUpdatedAt());
      }
  }
  
  @Override
   public int hashCode() {
    return new StringBuilder()
      .append(getId())
      .append(getEventId())
      .append(getUserId())
      .append(getAttended())
      .append(getAttendedDateTime())
      .append(getMandatoryEvent())
      .append(getCreatedAt())
      .append(getUpdatedAt())
      .toString()
      .hashCode();
  }
  
  @Override
   public String toString() {
    return new StringBuilder()
      .append("Attendance {")
      .append("id=" + String.valueOf(getId()) + ", ")
      .append("eventID=" + String.valueOf(getEventId()) + ", ")
      .append("userID=" + String.valueOf(getUserId()) + ", ")
      .append("attended=" + String.valueOf(getAttended()) + ", ")
      .append("attendedDateTime=" + String.valueOf(getAttendedDateTime()) + ", ")
      .append("mandatoryEvent=" + String.valueOf(getMandatoryEvent()) + ", ")
      .append("createdAt=" + String.valueOf(getCreatedAt()) + ", ")
      .append("updatedAt=" + String.valueOf(getUpdatedAt()))
      .append("}")
      .toString();
  }
  
  public static BuildStep builder() {
      return new Builder();
  }
  
  /** 
   * WARNING: This method should not be used to build an instance of this object for a CREATE mutation.
   * This is a convenience method to return an instance of the object with only its ID populated
   * to be used in the context of a parameter in a delete mutation or referencing a foreign key
   * in a relationship.
   * @param id the id of the existing item this instance will represent
   * @return an instance of this model with only ID populated
   */
  public static Attendance justId(String id) {
    return new Attendance(
      id,
      null,
      null,
      null,
      null,
      null
    );
  }
  
  public CopyOfBuilder copyOfBuilder() {
    return new CopyOfBuilder(id,
      eventID,
      userID,
      attended,
      attendedDateTime,
      mandatoryEvent);
  }
  public interface BuildStep {
    Attendance build();
    BuildStep id(String id);
    BuildStep eventId(String eventId);
    BuildStep userId(String userId);
    BuildStep attended(Boolean attended);
    BuildStep attendedDateTime(Temporal.DateTime attendedDateTime);
    BuildStep mandatoryEvent(Boolean mandatoryEvent);
  }
  

  public static class Builder implements BuildStep {
    private String id;
    private String eventID;
    private String userID;
    private Boolean attended;
    private Temporal.DateTime attendedDateTime;
    private Boolean mandatoryEvent;
    @Override
     public Attendance build() {
        String id = this.id != null ? this.id : UUID.randomUUID().toString();
        
        return new Attendance(
          id,
          eventID,
          userID,
          attended,
          attendedDateTime,
          mandatoryEvent);
    }
    
    @Override
     public BuildStep eventId(String eventId) {
        this.eventID = eventId;
        return this;
    }
    
    @Override
     public BuildStep userId(String userId) {
        this.userID = userId;
        return this;
    }
    
    @Override
     public BuildStep attended(Boolean attended) {
        this.attended = attended;
        return this;
    }
    
    @Override
     public BuildStep attendedDateTime(Temporal.DateTime attendedDateTime) {
        this.attendedDateTime = attendedDateTime;
        return this;
    }
    
    @Override
     public BuildStep mandatoryEvent(Boolean mandatoryEvent) {
        this.mandatoryEvent = mandatoryEvent;
        return this;
    }
    
    /** 
     * @param id id
     * @return Current Builder instance, for fluent method chaining
     */
    public BuildStep id(String id) {
        this.id = id;
        return this;
    }
  }
  

  public final class CopyOfBuilder extends Builder {
    private CopyOfBuilder(String id, String eventId, String userId, Boolean attended, Temporal.DateTime attendedDateTime, Boolean mandatoryEvent) {
      super.id(id);
      super.eventId(eventId)
        .userId(userId)
        .attended(attended)
        .attendedDateTime(attendedDateTime)
        .mandatoryEvent(mandatoryEvent);
    }
    
    @Override
     public CopyOfBuilder eventId(String eventId) {
      return (CopyOfBuilder) super.eventId(eventId);
    }
    
    @Override
     public CopyOfBuilder userId(String userId) {
      return (CopyOfBuilder) super.userId(userId);
    }
    
    @Override
     public CopyOfBuilder attended(Boolean attended) {
      return (CopyOfBuilder) super.attended(attended);
    }
    
    @Override
     public CopyOfBuilder attendedDateTime(Temporal.DateTime attendedDateTime) {
      return (CopyOfBuilder) super.attendedDateTime(attendedDateTime);
    }
    
    @Override
     public CopyOfBuilder mandatoryEvent(Boolean mandatoryEvent) {
      return (CopyOfBuilder) super.mandatoryEvent(mandatoryEvent);
    }
  }
  
}
