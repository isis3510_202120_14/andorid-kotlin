package com.amplifyframework.datastore.generated.model;

import com.amplifyframework.core.model.annotations.HasMany;
import com.amplifyframework.core.model.temporal.Temporal;

import java.util.List;
import java.util.UUID;
import java.util.Objects;

import androidx.core.util.ObjectsCompat;

import com.amplifyframework.core.model.AuthStrategy;
import com.amplifyframework.core.model.Model;
import com.amplifyframework.core.model.ModelOperation;
import com.amplifyframework.core.model.annotations.AuthRule;
import com.amplifyframework.core.model.annotations.Index;
import com.amplifyframework.core.model.annotations.ModelConfig;
import com.amplifyframework.core.model.annotations.ModelField;
import com.amplifyframework.core.model.query.predicate.QueryField;

import static com.amplifyframework.core.model.query.predicate.QueryField.field;

/** This is an auto generated class representing the Event type in your schema. */
@SuppressWarnings("all")
@ModelConfig(pluralName = "Events", authRules = {
  @AuthRule(allow = AuthStrategy.PRIVATE, operations = { ModelOperation.CREATE, ModelOperation.UPDATE, ModelOperation.DELETE, ModelOperation.READ })
})
public final class Event implements Model {
  public static final QueryField ID = field("Event", "id");
  public static final QueryField DESCRIPTION = field("Event", "description");
  public static final QueryField NAME = field("Event", "name");
  public static final QueryField START_DATE_TIME = field("Event", "startDateTime");
  public static final QueryField END_DATE_TIME = field("Event", "endDateTime");
  public static final QueryField QR_CODE = field("Event", "qrCode");
  public static final QueryField LATITUDE = field("Event", "latitude");
  public static final QueryField LONGITUDE = field("Event", "longitude");
  public static final QueryField ONLINE = field("Event", "online");
  public static final QueryField TAGS = field("Event", "tags");
  public static final QueryField LOCATION = field("Event", "location");
  public static final QueryField IS_PUBLIC = field("Event", "isPublic");
  private final @ModelField(targetType="ID", isRequired = true) String id;
  private final @ModelField(targetType="Attendance") @HasMany(associatedWith = "eventID", type = Attendance.class) List<Attendance> attendances = null;
  private final @ModelField(targetType="String") String description;
  private final @ModelField(targetType="String") String name;
  private final @ModelField(targetType="AWSDateTime") Temporal.DateTime startDateTime;
  private final @ModelField(targetType="AWSDateTime") Temporal.DateTime endDateTime;
  private final @ModelField(targetType="EventMedia") @HasMany(associatedWith = "event", type = EventMedia.class) List<EventMedia> media = null;
  private final @ModelField(targetType="AWSURL") String qrCode;
  private final @ModelField(targetType="Float") Double latitude;
  private final @ModelField(targetType="Float") Double longitude;
  private final @ModelField(targetType="Boolean") Boolean online;
  private final @ModelField(targetType="String") List<String> tags;
  private final @ModelField(targetType="String") String location;
  private final @ModelField(targetType="Boolean") Boolean isPublic;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime createdAt;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime updatedAt;
  public String getId() {
      return id;
  }
  
  public List<Attendance> getAttendances() {
      return attendances;
  }
  
  public String getDescription() {
      return description;
  }
  
  public String getName() {
      return name;
  }
  
  public Temporal.DateTime getStartDateTime() {
      return startDateTime;
  }
  
  public Temporal.DateTime getEndDateTime() {
      return endDateTime;
  }
  
  public List<EventMedia> getMedia() {
      return media;
  }
  
  public String getQrCode() {
      return qrCode;
  }
  
  public Double getLatitude() {
      return latitude;
  }
  
  public Double getLongitude() {
      return longitude;
  }
  
  public Boolean getOnline() {
      return online;
  }
  
  public List<String> getTags() {
      return tags;
  }
  
  public String getLocation() {
      return location;
  }
  
  public Boolean getIsPublic() {
      return isPublic;
  }
  
  public Temporal.DateTime getCreatedAt() {
      return createdAt;
  }
  
  public Temporal.DateTime getUpdatedAt() {
      return updatedAt;
  }
  
  private Event(String id, String description, String name, Temporal.DateTime startDateTime, Temporal.DateTime endDateTime, String qrCode, Double latitude, Double longitude, Boolean online, List<String> tags, String location, Boolean isPublic) {
    this.id = id;
    this.description = description;
    this.name = name;
    this.startDateTime = startDateTime;
    this.endDateTime = endDateTime;
    this.qrCode = qrCode;
    this.latitude = latitude;
    this.longitude = longitude;
    this.online = online;
    this.tags = tags;
    this.location = location;
    this.isPublic = isPublic;
  }
  
  @Override
   public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      } else if(obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
      Event event = (Event) obj;
      return ObjectsCompat.equals(getId(), event.getId()) &&
              ObjectsCompat.equals(getDescription(), event.getDescription()) &&
              ObjectsCompat.equals(getName(), event.getName()) &&
              ObjectsCompat.equals(getStartDateTime(), event.getStartDateTime()) &&
              ObjectsCompat.equals(getEndDateTime(), event.getEndDateTime()) &&
              ObjectsCompat.equals(getQrCode(), event.getQrCode()) &&
              ObjectsCompat.equals(getLatitude(), event.getLatitude()) &&
              ObjectsCompat.equals(getLongitude(), event.getLongitude()) &&
              ObjectsCompat.equals(getOnline(), event.getOnline()) &&
              ObjectsCompat.equals(getTags(), event.getTags()) &&
              ObjectsCompat.equals(getLocation(), event.getLocation()) &&
              ObjectsCompat.equals(getIsPublic(), event.getIsPublic()) &&
              ObjectsCompat.equals(getCreatedAt(), event.getCreatedAt()) &&
              ObjectsCompat.equals(getUpdatedAt(), event.getUpdatedAt());
      }
  }
  
  @Override
   public int hashCode() {
    return new StringBuilder()
      .append(getId())
      .append(getDescription())
      .append(getName())
      .append(getStartDateTime())
      .append(getEndDateTime())
      .append(getQrCode())
      .append(getLatitude())
      .append(getLongitude())
      .append(getOnline())
      .append(getTags())
      .append(getLocation())
      .append(getIsPublic())
      .append(getCreatedAt())
      .append(getUpdatedAt())
      .toString()
      .hashCode();
  }
  
  @Override
   public String toString() {
    return new StringBuilder()
      .append("Event {")
      .append("id=" + String.valueOf(getId()) + ", ")
      .append("description=" + String.valueOf(getDescription()) + ", ")
      .append("name=" + String.valueOf(getName()) + ", ")
      .append("startDateTime=" + String.valueOf(getStartDateTime()) + ", ")
      .append("endDateTime=" + String.valueOf(getEndDateTime()) + ", ")
      .append("qrCode=" + String.valueOf(getQrCode()) + ", ")
      .append("latitude=" + String.valueOf(getLatitude()) + ", ")
      .append("longitude=" + String.valueOf(getLongitude()) + ", ")
      .append("online=" + String.valueOf(getOnline()) + ", ")
      .append("tags=" + String.valueOf(getTags()) + ", ")
      .append("location=" + String.valueOf(getLocation()) + ", ")
      .append("isPublic=" + String.valueOf(getIsPublic()) + ", ")
      .append("createdAt=" + String.valueOf(getCreatedAt()) + ", ")
      .append("updatedAt=" + String.valueOf(getUpdatedAt()))
      .append("}")
      .toString();
  }
  
  public static BuildStep builder() {
      return new Builder();
  }
  
  /** 
   * WARNING: This method should not be used to build an instance of this object for a CREATE mutation.
   * This is a convenience method to return an instance of the object with only its ID populated
   * to be used in the context of a parameter in a delete mutation or referencing a foreign key
   * in a relationship.
   * @param id the id of the existing item this instance will represent
   * @return an instance of this model with only ID populated
   */
  public static Event justId(String id) {
    return new Event(
      id,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }
  
  public CopyOfBuilder copyOfBuilder() {
    return new CopyOfBuilder(id,
      description,
      name,
      startDateTime,
      endDateTime,
      qrCode,
      latitude,
      longitude,
      online,
      tags,
      location,
      isPublic);
  }
  public interface BuildStep {
    Event build();
    BuildStep id(String id);
    BuildStep description(String description);
    BuildStep name(String name);
    BuildStep startDateTime(Temporal.DateTime startDateTime);
    BuildStep endDateTime(Temporal.DateTime endDateTime);
    BuildStep qrCode(String qrCode);
    BuildStep latitude(Double latitude);
    BuildStep longitude(Double longitude);
    BuildStep online(Boolean online);
    BuildStep tags(List<String> tags);
    BuildStep location(String location);
    BuildStep isPublic(Boolean isPublic);
  }
  

  public static class Builder implements BuildStep {
    private String id;
    private String description;
    private String name;
    private Temporal.DateTime startDateTime;
    private Temporal.DateTime endDateTime;
    private String qrCode;
    private Double latitude;
    private Double longitude;
    private Boolean online;
    private List<String> tags;
    private String location;
    private Boolean isPublic;
    @Override
     public Event build() {
        String id = this.id != null ? this.id : UUID.randomUUID().toString();
        
        return new Event(
          id,
          description,
          name,
          startDateTime,
          endDateTime,
          qrCode,
          latitude,
          longitude,
          online,
          tags,
          location,
          isPublic);
    }
    
    @Override
     public BuildStep description(String description) {
        this.description = description;
        return this;
    }
    
    @Override
     public BuildStep name(String name) {
        this.name = name;
        return this;
    }
    
    @Override
     public BuildStep startDateTime(Temporal.DateTime startDateTime) {
        this.startDateTime = startDateTime;
        return this;
    }
    
    @Override
     public BuildStep endDateTime(Temporal.DateTime endDateTime) {
        this.endDateTime = endDateTime;
        return this;
    }
    
    @Override
     public BuildStep qrCode(String qrCode) {
        this.qrCode = qrCode;
        return this;
    }
    
    @Override
     public BuildStep latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }
    
    @Override
     public BuildStep longitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }
    
    @Override
     public BuildStep online(Boolean online) {
        this.online = online;
        return this;
    }
    
    @Override
     public BuildStep tags(List<String> tags) {
        this.tags = tags;
        return this;
    }
    
    @Override
     public BuildStep location(String location) {
        this.location = location;
        return this;
    }
    
    @Override
     public BuildStep isPublic(Boolean isPublic) {
        this.isPublic = isPublic;
        return this;
    }
    
    /** 
     * @param id id
     * @return Current Builder instance, for fluent method chaining
     */
    public BuildStep id(String id) {
        this.id = id;
        return this;
    }
  }
  

  public final class CopyOfBuilder extends Builder {
    private CopyOfBuilder(String id, String description, String name, Temporal.DateTime startDateTime, Temporal.DateTime endDateTime, String qrCode, Double latitude, Double longitude, Boolean online, List<String> tags, String location, Boolean isPublic) {
      super.id(id);
      super.description(description)
        .name(name)
        .startDateTime(startDateTime)
        .endDateTime(endDateTime)
        .qrCode(qrCode)
        .latitude(latitude)
        .longitude(longitude)
        .online(online)
        .tags(tags)
        .location(location)
        .isPublic(isPublic);
    }
    
    @Override
     public CopyOfBuilder description(String description) {
      return (CopyOfBuilder) super.description(description);
    }
    
    @Override
     public CopyOfBuilder name(String name) {
      return (CopyOfBuilder) super.name(name);
    }
    
    @Override
     public CopyOfBuilder startDateTime(Temporal.DateTime startDateTime) {
      return (CopyOfBuilder) super.startDateTime(startDateTime);
    }
    
    @Override
     public CopyOfBuilder endDateTime(Temporal.DateTime endDateTime) {
      return (CopyOfBuilder) super.endDateTime(endDateTime);
    }
    
    @Override
     public CopyOfBuilder qrCode(String qrCode) {
      return (CopyOfBuilder) super.qrCode(qrCode);
    }
    
    @Override
     public CopyOfBuilder latitude(Double latitude) {
      return (CopyOfBuilder) super.latitude(latitude);
    }
    
    @Override
     public CopyOfBuilder longitude(Double longitude) {
      return (CopyOfBuilder) super.longitude(longitude);
    }
    
    @Override
     public CopyOfBuilder online(Boolean online) {
      return (CopyOfBuilder) super.online(online);
    }
    
    @Override
     public CopyOfBuilder tags(List<String> tags) {
      return (CopyOfBuilder) super.tags(tags);
    }
    
    @Override
     public CopyOfBuilder location(String location) {
      return (CopyOfBuilder) super.location(location);
    }
    
    @Override
     public CopyOfBuilder isPublic(Boolean isPublic) {
      return (CopyOfBuilder) super.isPublic(isPublic);
    }
  }
  
}
