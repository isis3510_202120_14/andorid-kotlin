package com.amplifyframework.datastore.generated.model;
/** Auto generated enum from GraphQL schema. */
@SuppressWarnings("all")
public enum TaskType {
  CAPACITACION,
  PLANEACION,
  CONSTRUCCION_VIVIENDAS,
  EJECUCION_ACTIVIDAD
}
