package com.amplifyframework.datastore.generated.model;

import com.amplifyframework.core.model.annotations.HasMany;
import com.amplifyframework.core.model.temporal.Temporal;

import java.util.List;
import java.util.UUID;
import java.util.Objects;

import androidx.core.util.ObjectsCompat;

import com.amplifyframework.core.model.AuthStrategy;
import com.amplifyframework.core.model.Model;
import com.amplifyframework.core.model.ModelOperation;
import com.amplifyframework.core.model.annotations.AuthRule;
import com.amplifyframework.core.model.annotations.Index;
import com.amplifyframework.core.model.annotations.ModelConfig;
import com.amplifyframework.core.model.annotations.ModelField;
import com.amplifyframework.core.model.query.predicate.QueryField;

import static com.amplifyframework.core.model.query.predicate.QueryField.field;

/** This is an auto generated class representing the User type in your schema. */
@SuppressWarnings("all")
@ModelConfig(pluralName = "Users", authRules = {
  @AuthRule(allow = AuthStrategy.OWNER, ownerField = "owner", identityClaim = "cognito:username", provider = "userPools", operations = { ModelOperation.CREATE, ModelOperation.DELETE, ModelOperation.UPDATE })
})
public final class User implements Model {
  public static final QueryField ID = field("User", "id");
  public static final QueryField NAME = field("User", "name");
  public static final QueryField EMAIL = field("User", "email");
  public static final QueryField ROLE = field("User", "role");
  public static final QueryField CITY = field("User", "city");
  public static final QueryField BIRTHDAY = field("User", "birthday");
  public static final QueryField DNI = field("User", "dni");
  public static final QueryField PHONE = field("User", "phone");
  private final @ModelField(targetType="ID", isRequired = true) String id;
  private final @ModelField(targetType="String") String name;
  private final @ModelField(targetType="String") String email;
  private final @ModelField(targetType="UserRole") UserRole role;
  private final @ModelField(targetType="Attendance") @HasMany(associatedWith = "userID", type = Attendance.class) List<Attendance> attendances = null;
  private final @ModelField(targetType="String") String city;
  private final @ModelField(targetType="AWSDate") Temporal.Date birthday;
  private final @ModelField(targetType="String") String dni;
  private final @ModelField(targetType="AWSPhone") String phone;
  private final @ModelField(targetType="Task") @HasMany(associatedWith = "userID", type = Task.class) List<Task> tasks = null;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime createdAt;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime updatedAt;
  public String getId() {
      return id;
  }
  
  public String getName() {
      return name;
  }
  
  public String getEmail() {
      return email;
  }
  
  public UserRole getRole() {
      return role;
  }
  
  public List<Attendance> getAttendances() {
      return attendances;
  }
  
  public String getCity() {
      return city;
  }
  
  public Temporal.Date getBirthday() {
      return birthday;
  }
  
  public String getDni() {
      return dni;
  }
  
  public String getPhone() {
      return phone;
  }
  
  public List<Task> getTasks() {
      return tasks;
  }
  
  public Temporal.DateTime getCreatedAt() {
      return createdAt;
  }
  
  public Temporal.DateTime getUpdatedAt() {
      return updatedAt;
  }
  
  private User(String id, String name, String email, UserRole role, String city, Temporal.Date birthday, String dni, String phone) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.role = role;
    this.city = city;
    this.birthday = birthday;
    this.dni = dni;
    this.phone = phone;
  }
  
  @Override
   public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      } else if(obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
      User user = (User) obj;
      return ObjectsCompat.equals(getId(), user.getId()) &&
              ObjectsCompat.equals(getName(), user.getName()) &&
              ObjectsCompat.equals(getEmail(), user.getEmail()) &&
              ObjectsCompat.equals(getRole(), user.getRole()) &&
              ObjectsCompat.equals(getCity(), user.getCity()) &&
              ObjectsCompat.equals(getBirthday(), user.getBirthday()) &&
              ObjectsCompat.equals(getDni(), user.getDni()) &&
              ObjectsCompat.equals(getPhone(), user.getPhone()) &&
              ObjectsCompat.equals(getCreatedAt(), user.getCreatedAt()) &&
              ObjectsCompat.equals(getUpdatedAt(), user.getUpdatedAt());
      }
  }
  
  @Override
   public int hashCode() {
    return new StringBuilder()
      .append(getId())
      .append(getName())
      .append(getEmail())
      .append(getRole())
      .append(getCity())
      .append(getBirthday())
      .append(getDni())
      .append(getPhone())
      .append(getCreatedAt())
      .append(getUpdatedAt())
      .toString()
      .hashCode();
  }
  
  @Override
   public String toString() {
    return new StringBuilder()
      .append("User {")
      .append("id=" + String.valueOf(getId()) + ", ")
      .append("name=" + String.valueOf(getName()) + ", ")
      .append("email=" + String.valueOf(getEmail()) + ", ")
      .append("role=" + String.valueOf(getRole()) + ", ")
      .append("city=" + String.valueOf(getCity()) + ", ")
      .append("birthday=" + String.valueOf(getBirthday()) + ", ")
      .append("dni=" + String.valueOf(getDni()) + ", ")
      .append("phone=" + String.valueOf(getPhone()) + ", ")
      .append("createdAt=" + String.valueOf(getCreatedAt()) + ", ")
      .append("updatedAt=" + String.valueOf(getUpdatedAt()))
      .append("}")
      .toString();
  }
  
  public static BuildStep builder() {
      return new Builder();
  }
  
  /** 
   * WARNING: This method should not be used to build an instance of this object for a CREATE mutation.
   * This is a convenience method to return an instance of the object with only its ID populated
   * to be used in the context of a parameter in a delete mutation or referencing a foreign key
   * in a relationship.
   * @param id the id of the existing item this instance will represent
   * @return an instance of this model with only ID populated
   */
  public static User justId(String id) {
    return new User(
      id,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }
  
  public CopyOfBuilder copyOfBuilder() {
    return new CopyOfBuilder(id,
      name,
      email,
      role,
      city,
      birthday,
      dni,
      phone);
  }
  public interface BuildStep {
    User build();
    BuildStep id(String id);
    BuildStep name(String name);
    BuildStep email(String email);
    BuildStep role(UserRole role);
    BuildStep city(String city);
    BuildStep birthday(Temporal.Date birthday);
    BuildStep dni(String dni);
    BuildStep phone(String phone);
  }
  

  public static class Builder implements BuildStep {
    private String id;
    private String name;
    private String email;
    private UserRole role;
    private String city;
    private Temporal.Date birthday;
    private String dni;
    private String phone;
    @Override
     public User build() {
        String id = this.id != null ? this.id : UUID.randomUUID().toString();
        
        return new User(
          id,
          name,
          email,
          role,
          city,
          birthday,
          dni,
          phone);
    }
    
    @Override
     public BuildStep name(String name) {
        this.name = name;
        return this;
    }
    
    @Override
     public BuildStep email(String email) {
        this.email = email;
        return this;
    }
    
    @Override
     public BuildStep role(UserRole role) {
        this.role = role;
        return this;
    }
    
    @Override
     public BuildStep city(String city) {
        this.city = city;
        return this;
    }
    
    @Override
     public BuildStep birthday(Temporal.Date birthday) {
        this.birthday = birthday;
        return this;
    }
    
    @Override
     public BuildStep dni(String dni) {
        this.dni = dni;
        return this;
    }
    
    @Override
     public BuildStep phone(String phone) {
        this.phone = phone;
        return this;
    }
    
    /** 
     * @param id id
     * @return Current Builder instance, for fluent method chaining
     */
    public BuildStep id(String id) {
        this.id = id;
        return this;
    }
  }
  

  public final class CopyOfBuilder extends Builder {
    private CopyOfBuilder(String id, String name, String email, UserRole role, String city, Temporal.Date birthday, String dni, String phone) {
      super.id(id);
      super.name(name)
        .email(email)
        .role(role)
        .city(city)
        .birthday(birthday)
        .dni(dni)
        .phone(phone);
    }
    
    @Override
     public CopyOfBuilder name(String name) {
      return (CopyOfBuilder) super.name(name);
    }
    
    @Override
     public CopyOfBuilder email(String email) {
      return (CopyOfBuilder) super.email(email);
    }
    
    @Override
     public CopyOfBuilder role(UserRole role) {
      return (CopyOfBuilder) super.role(role);
    }
    
    @Override
     public CopyOfBuilder city(String city) {
      return (CopyOfBuilder) super.city(city);
    }
    
    @Override
     public CopyOfBuilder birthday(Temporal.Date birthday) {
      return (CopyOfBuilder) super.birthday(birthday);
    }
    
    @Override
     public CopyOfBuilder dni(String dni) {
      return (CopyOfBuilder) super.dni(dni);
    }
    
    @Override
     public CopyOfBuilder phone(String phone) {
      return (CopyOfBuilder) super.phone(phone);
    }
  }
  
}
