package com.example.androidkotlin

import android.app.Application
import com.example.androidkotlin.data.PreferencesRepository
import com.example.androidkotlin.data.events.EventsDataSource
import com.example.androidkotlin.data.events.EventsRepository
import com.example.androidkotlin.data.login.LoginDataSource
import com.example.androidkotlin.data.login.LoginRepository
import com.example.androidkotlin.data.tasks.TasksDataSource
import com.example.androidkotlin.data.tasks.TasksRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class TechoApp : Application(){

    // No need to cancel this scope as it'll be torn down with the process
    val applicationScope = CoroutineScope(SupervisorJob())
    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    val loginRepository by lazy { LoginRepository.getInstance(LoginDataSource(this)) }
    val eventsRepository by lazy { EventsRepository.getInstance(EventsDataSource()) }
    val tasksRepository by lazy { TasksRepository.getInstance(TasksDataSource()) }

    val preferencesRepository by lazy { PreferencesRepository(this) }
}