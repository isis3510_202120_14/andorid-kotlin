package com.example.androidkotlin.adapters

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.TaskItemBinding
import com.example.androidkotlin.ui.tasks.TaskSheetFragment
import com.example.androidkotlin.ui.tasks.TaskUserView
import com.example.androidkotlin.ui.tasks.TasksFragment
import java.time.format.TextStyle
import java.util.*

class TasksAdapter (val tasksFragment: TasksFragment, val tasks : ArrayList<TaskUserView>) : RecyclerView.Adapter<TasksAdapter.ViewHolder>() {

    class ViewHolder(val taskItemBinding : TaskItemBinding): RecyclerView.ViewHolder(taskItemBinding.root){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val taskItemBinding = TaskItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(taskItemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = tasks[position]
        holder.taskItemBinding.task = task

        val locale = Locale("es", "CO")
        holder.taskItemBinding.day.text =
            task.dueDate.dayOfWeek.getDisplayName(TextStyle.SHORT, locale)
                .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }.split(".")[0]
        holder.taskItemBinding.date.text = task.dueDate.dayOfMonth.toString()
        holder.taskItemBinding.month.text = task.dueDate.month.getDisplayName(TextStyle.SHORT, locale)
            .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }.split(".")[0]
        holder.taskItemBinding.time.text = task.dueDate.toLocalTime().toString()

        holder.taskItemBinding.type.text =  task.type
        holder.taskItemBinding.status.text = if (task.isCompleted) "COMPLETED" else "UPCOMING"

        holder.taskItemBinding.options.setOnClickListener { view: View? ->
            showPopUpMenu(view, position)
        }

    }

    private fun showPopUpMenu(view: View?, position: Int) {
        val task = tasks[position]

        val popupMenu = PopupMenu(tasksFragment.requireActivity(), view)
        popupMenu.menuInflater.inflate(R.menu.task_options, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item: MenuItem ->
            when (item.itemId) {
                R.id.menuDelete -> {
                    val alertDialogBuilder = AlertDialog.Builder(tasksFragment.requireActivity(), R.style.Theme_AndroidKotlin)
                    alertDialogBuilder.setTitle(R.string.confirmation_dialog)
                        .setMessage(R.string.task_delete)
                        .setPositiveButton(R.string.yes) { _, _ -> deleteTaskFromId(task.id,position) }
                        .setNegativeButton(R.string.no) { dialog, _ -> dialog.cancel() }.show()
                }
                R.id.menuUpdate -> {
                    val createTaskBottomSheetFragment = TaskSheetFragment()
                    createTaskBottomSheetFragment.setTaskId(task.id, true, tasksFragment)
                    createTaskBottomSheetFragment.setTaskItem(task)
                    createTaskBottomSheetFragment.show(tasksFragment.requireActivity().supportFragmentManager,createTaskBottomSheetFragment.tag)
                }
                R.id.menuComplete -> {
                    val completeAlertDialog = AlertDialog.Builder(tasksFragment.requireActivity(), R.style.Theme_AndroidKotlin)
                    completeAlertDialog.setTitle(R.string.task_completed)
                        .setMessage(R.string.sureToMarkAsComplete)
                        .setPositiveButton(R.string.yes) { _, _ -> showCompleteDialog(task.id,position) }
                        .setNegativeButton(R.string.no) { dialog, _ -> dialog.cancel() }.show()
                }
            }
            false
        }
        popupMenu.show()
    }

    private fun showCompleteDialog(taskId: String, position: Int) {
        val dialog = Dialog(tasksFragment.requireContext(), R.style.Theme_AndroidKotlin)
        dialog.setContentView(R.layout.dialog_completed_task)

        val close = dialog.findViewById<Button>(R.id.closeButton)
        close.setOnClickListener {
            tasksFragment.completeTask(taskId, tasks[position])
            removeAtPosition(position)
            dialog.dismiss()
        }

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }


    private fun deleteTaskFromId(taskId: String, position: Int) {
        tasksFragment.deleteTask(taskId, position)
        removeAtPosition(position)
    }

    fun removeAtPosition(position: Int) {
        tasks.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, tasks.size)
    }

    override fun getItemCount(): Int {
        return tasks.size
    }
}
