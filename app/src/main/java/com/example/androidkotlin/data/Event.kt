package com.example.androidkotlin.data

import java.util.*

data class Event(
    val id: String,
    val name: String,
    val description: String,
    val startDateTime: Date,
    val endDateTime: Date,
    val qrCode: String,
    val latitude: Float,
    val longitude :Float,
    val online: Boolean,
    val tags: List<String>,
    val location: String,
) {
    override fun toString() = "Evento - Nombre: $name, id: $id"
}