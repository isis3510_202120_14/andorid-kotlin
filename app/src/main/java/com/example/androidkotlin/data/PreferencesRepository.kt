
package com.example.androidkotlin.data

import android.content.Context
import android.content.SharedPreferences
import com.example.androidkotlin.data.login.LoginDataSource
import com.example.androidkotlin.data.login.LoginRepository
import com.example.androidkotlin.ui.login.LoginUserView
import com.example.androidkotlin.ui.login.LoginViewModel
import com.google.gson.Gson

const val PREFERENCE_NAME = "TECHO_APP"

// Variables
const val SIGN_UP_FORM = "SIGN_UP_FORM"

const val THEME_MODE = "THEME_MODE"



class PreferencesRepository (val context: Context) {

    companion object {
        const val DARK_MODE = "dark"
        const val LIGHT_MODE = "light"
        const val DEFAULT_MODE = "default"
    }

    private val pref: SharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    private val editor = pref.edit()

    private val gson = Gson()

    private fun String.put(long: Long) {
        editor.putLong(this, long)
        editor.commit()
    }

    private fun String.put(int: Int) {
        editor.putInt(this, int)
        editor.commit()
    }

    private fun String.put(string: String) {
        editor.putString(this, string)
        editor.commit()
    }

    private fun String.put(boolean: Boolean) {
        editor.putBoolean(this, boolean)
        editor.commit()
    }

    private fun String.getLong() = pref.getLong(this, 0)

    private fun String.getInt() = pref.getInt(this, 0)

    private fun String.getString() = pref.getString(this, "")!!

    private fun String.getBoolean() = pref.getBoolean(this, false)

    fun clearData() {
        editor.clear()
        editor.commit()
    }

    fun saveSignupUser(userInfo: LoginUserView){
        SIGN_UP_FORM.put(gson.toJson(userInfo))
    }

    fun getSignupUser(): LoginUserView?{
        SIGN_UP_FORM.getString().also {
            return if (it.isNotEmpty())
                gson.fromJson(it, LoginUserView::class.java)
            else
                null
        }
    }

    fun cleanSignupUser(){
        editor.remove(SIGN_UP_FORM)
        editor.commit()
    }

    fun saveThemeMode( themeMode : String){
        THEME_MODE.put(themeMode)
    }

    fun getThemeMode(): String {
        THEME_MODE.getString().also {
            return if (it.isNotEmpty())
                it
            else
                LIGHT_MODE
        }
    }

}