package com.example.androidkotlin.data.events

import com.amplifyframework.datastore.generated.model.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import com.example.androidkotlin.data.Result

class EventsRepository(val dataSource: EventsDataSource) {

    private val scope = CoroutineScope(Job() + Dispatchers.IO)

    var events: List<Event>? = null
        private set

    // Singleton object
    companion object {
        @Volatile
        private var instance: EventsRepository? = null

        fun getInstance(dataSource: EventsDataSource) = instance ?: synchronized(this) {
            instance ?: EventsRepository(dataSource).also { instance = it }
        }
    }
    // Get all events
    suspend fun getEvents(): Result<List<Event>> {
        val result = dataSource.getEvents()
        if (result is Result.Success) this.events = result.data
        return result
    }
    // Get an event by id
    suspend fun getEvent(id: String): Result<Event> {
        return dataSource.getEvent(id)
    }
    // Get an event by id
    suspend fun takeAttendance(pLat: String, pLong: String, qrCode: String, userId:String): Result<String> {
        return dataSource.takeAttendance(pLat,pLong,qrCode,userId)
    }
}