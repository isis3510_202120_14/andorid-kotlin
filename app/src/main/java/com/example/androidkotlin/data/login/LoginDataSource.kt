package com.example.androidkotlin.data.login

import android.content.Context
import android.util.Log
import com.amplifyframework.AmplifyException
import com.amplifyframework.api.aws.AWSApiPlugin
import com.amplifyframework.auth.AuthException
import com.amplifyframework.auth.AuthUserAttributeKey
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin
import com.amplifyframework.auth.options.AuthSignUpOptions
import com.amplifyframework.kotlin.core.Amplify as KotlinAmplify
import com.amplifyframework.core.Amplify
import com.amplifyframework.core.model.query.Where
import com.amplifyframework.core.model.temporal.Temporal
import com.amplifyframework.datastore.AWSDataStorePlugin
import com.amplifyframework.datastore.generated.model.User
import com.amplifyframework.datastore.generated.model.UserRole
import com.example.androidkotlin.data.Result
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import java.io.IOException
import java.lang.Exception
import java.util.*

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource(context: Context) {
    private var user: User? = null

    init {
        try {
            Amplify.addPlugin(AWSDataStorePlugin())
            Amplify.addPlugin(AWSApiPlugin())
            Amplify.addPlugin(AWSCognitoAuthPlugin())
            Amplify.configure(context)
            Log.i("MyAmplifyApp", "Amplify configured")
        } catch (error: AmplifyException) {
            Log.e("MyAmplifyApp", "Could not initialize Amplify", error)
        }
    }

    suspend fun login(username: String, password: String): Result<User> {
        try {
            val result = KotlinAmplify.Auth.signIn(username, password)
            if (result.isSignInComplete) {
                Log.i("AuthQuickstart", "Sign in succeeded")

                var user = User.builder().email(username).build()

                KotlinAmplify.DataStore
                    .query(User::class, Where.matches(User.EMAIL.eq(username)))
                    .catch { Log.e("MyAmplifyApp", "Query failed", it) }
                    .collect {
                        user = it
                        Log.i("MyAmplifyApp", "Post: $it")
                    }

                return Result.Success(user)
            } else {
                Log.i("AuthQuickstart", "Sign in not complete")
                return Result.Error(Exception("Sign in not complete"))
            }
        } catch (error: Exception) {
            Log.e("AuthQuickstart", error.toString(), error)
            return Result.Error(IOException(error.message, error))
        }
    }

    suspend fun signUp(
        username: String,
        fullname: String,
        document: String,
        phone: String,
        birthday: Date,
        city: String,
        password: String
    ): Result<User> {

        val options = AuthSignUpOptions.builder()
            .userAttribute(AuthUserAttributeKey.email(), username)
            .build()
        return try {
            Log.i("AuthQuickStart", "Before signup")

            val result = KotlinAmplify.Auth.signUp(username, password, options)
            Log.i("AuthQuickStart", "Sign up succeeded: $result")

            val user = User.builder()
                .email(username)
                .name(fullname)
                .dni(document)
                .phone(phone)
                .birthday(Temporal.Date(birthday))
                .city(city)
                .role(UserRole.VOLUNTEER)
                .build()

            Amplify.DataStore.save(user,
                { Log.i("DatastoreAmplify", "Saved the user ${it.item().email}") },
                { Log.e("DatastoreAmplify", it.message, it) }
            )
            Result.Success(user)

        } catch (error: Exception) {
            Log.e("AuthQuickStart", error.message, error)

            Result.Error(IOException(error.message, error))
        }

    }

    fun logout(){
        Amplify.Auth.signOut(
            { Log.i("AuthQuickstart", "Signed out successfully") },
            { Log.e("AuthQuickstart", "Sign out failed", it) }
        )
    }

    suspend fun confirmSignIn(username: String, password: String, confirmationCode: String): Result<User> {
        try {
            val result = KotlinAmplify.Auth.confirmSignUp(username, confirmationCode)
            if (result.isSignUpComplete) {

                Log.i("AuthQuickstart", "Signup confirmed")
                login(username, password)

                var user = User.builder()
                    .email(username)
                    .role(UserRole.VOLUNTEER)
                    .build()

                KotlinAmplify.DataStore
                    .query(User::class, Where.matches(User.EMAIL.eq(username)))
                    .catch { Log.e("MyAmplifyApp", "Query failed", it) }
                    .collect {
                        user = it
                        Log.i("MyAmplifyApp", "Post: $it")
                    }

                return Result.Success(user)
            } else {
                Log.i("AuthQuickstart", "Signup confirmation not yet complete")

                return Result.Error(Exception("Signup confirmation not yet complete"))
            }
        } catch (error: AuthException) {
            Log.e("AuthQuickstart", "Failed to confirm signup", error)
            return Result.Error(IOException(error.message, error))
        }
    }

    suspend fun getUser(): User? {
        return when (Amplify.Auth.currentUser) {
            null -> {
                null
            }
            else -> {
                val username = Amplify.Auth.currentUser.username

                user = User.builder().email(username).build()
                KotlinAmplify.DataStore
                    .query(User::class, Where.matches(User.EMAIL.eq(username)))
                    .catch { Log.e("MyAmplifyApp", "Query failed", it) }
                    .collect {
                        user = it
                        Log.i("MyAmplifyApp", "Post: $it")
                    }
                user
            }
        }

    }

    //Pre: El usuario ya esta logueado
    suspend fun getUserProfile( email: String ): Result<User>{
        try{
            var user:User = User.builder().build()
            var error: Result.Error? = null
            KotlinAmplify.DataStore
                .query(User::class, Where.matches(User.EMAIL.eq(email)))
                .catch {
                    Log.e("MyAmplifyApp", "Query failed", it)
                    error = Result.Error(Exception(it.message))
                }
                .collect {
                    user = it
                    Log.i("MyAmplifyApp", "Post: $it")
                }
            if( error != null ){
                error!!.exception.message?.let{
                    Log.i("MyAmplifyApp/getUser",it)
                }
                return error as Result.Error
            }
            return Result.Success(user)
        } catch(e: Exception) {
            return Result.Error(e)
        }
    }

    suspend fun getUserInfo():Result<User>{
        try{

            var useratt = Amplify.Auth.currentUser.username
            var user:User = User.builder().build()
            var error: Result.Error? = null

            KotlinAmplify.DataStore
                .query(User::class, Where.matches(User.EMAIL.eq(useratt)))
                .catch {
                    Log.e("MyAmplifyApp", "Query failed", it)
                    error = Result.Error(Exception(it.message))
                }
                .collect {
                    user = it
                    Log.i("MyAmplifyApp", "Post: $it")
                }
            if( error != null ){
                error!!.exception.message?.let{
                    Log.i("MyAmplifyApp/getUser",it)
                }
                return error as Result.Error
            }
            return Result.Success(user)
        } catch(e: Exception) {
            return Result.Error(e)
        }
    }

    fun getTemporalUser(): User?{
        return when (user){
            null ->
                when (Amplify.Auth.currentUser){
                    null -> null
                    else -> User.builder().email(Amplify.Auth.currentUser.username).build()
                }
            else -> user
        }
    }
}