package com.example.androidkotlin.data.login

import com.amplifyframework.datastore.generated.model.User
import com.example.androidkotlin.data.Result
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*

class LoginRepository(val dataSource: LoginDataSource) {
    private val scope = CoroutineScope(Job() + Dispatchers.IO)

    // Singleton object
    companion object {
        @Volatile
        private var instance: LoginRepository? = null

        fun getInstance(dataSource: LoginDataSource) = instance ?: synchronized(this) {
            instance ?: LoginRepository(dataSource).also { instance = it }
        }
    }

    // in-memory cache of the loggedInUser object
    var user: User? = null
        private set

    val isLoggedIn: Boolean
        get() = user != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        dataSource.getTemporalUser()?.let { setLoggedInUser(it) }
        if (user != null){
            scope.launch {
                dataSource.getUser()?.let { setLoggedInUser(it) }
            }
        }
    }

    fun logout() {
        user = null
        dataSource.logout()
    }


    suspend fun login(username: String, password: String): Result<User> {
        // handle login
        val result = dataSource.login(username, password)

        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }

        return result
    }

    suspend fun signUp(
        username: String,
        fullname: String,
        document: String,
        phone: String,
        birthday: Date,
        city: String,
        password: String
    ): Result<User> {
        // handle login
        val result = dataSource.signUp(username, fullname, document, phone, birthday, city, password)

        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }

        return result
    }

    suspend fun confirmSignUp(username: String, password: String, confirmationCode: String): Result<User> {
        val result = dataSource.confirmSignIn(username, password, confirmationCode)

        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }

        return result
    }

    suspend fun getUserProfile(): Result<User>{
        return dataSource.getUserInfo()
    }

    private fun setLoggedInUser(loggedInUser: User) {
        this.user = loggedInUser
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }

}