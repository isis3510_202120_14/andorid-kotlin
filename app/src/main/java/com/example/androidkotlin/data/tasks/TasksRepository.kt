package com.example.androidkotlin.data.tasks

import com.amplifyframework.datastore.generated.model.Task
import com.example.androidkotlin.data.Result

class TasksRepository(private val dataSource: TasksDataSource) {
    var tasks: List<Task>? = null
        private set

    companion object {
        @Volatile
        private var instance: TasksRepository? = null

        fun getInstance(dataSource: TasksDataSource) = instance ?: synchronized(this) {
            instance ?: TasksRepository(dataSource).also { instance = it }
        }
    }

    suspend fun getTasks(): Result<List<Task>> {
        val result = dataSource.getTasks()
        if (result is Result.Success) this.tasks = result.data
        return result
    }

    suspend fun getTask(id: String): Result<Task> {
        return dataSource.getTask(id)
    }

    suspend fun deleteTask(id: String): Result<Task> {
        return dataSource.deleteTask(id)
    }

    suspend fun createTask(task: Task): Result<Task>{
        return dataSource.createTask(task)
    }

    suspend fun updateTask(id: String, task: Task): Result<Task>{
        return dataSource.completeTask(id, task)
    }

    suspend fun completeTask(id: String, task: Task): Result<Task> {
        return dataSource.updateTask(id, task)
    }
}