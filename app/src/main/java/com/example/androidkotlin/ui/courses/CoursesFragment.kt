package com.example.androidkotlin.ui.courses


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.androidkotlin.R

class CoursesFragment : Fragment() {


  companion object {
    fun newInstance() = CoursesFragment()
  }

  private lateinit var viewModel: CoursesViewModel

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_courses, container, false)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    viewModel = ViewModelProvider(this).get(CoursesViewModel::class.java)
    // TODO: Use the ViewModel
  }
}