package com.example.androidkotlin.ui.events

import com.amplifyframework.datastore.generated.model.Event

data class AttendanceResult(
    val success: String? = null,
    val error: String? = null
)
