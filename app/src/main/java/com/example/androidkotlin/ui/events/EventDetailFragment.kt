package com.example.androidkotlin.ui.events

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.amplifyframework.core.model.temporal.Temporal
import com.amplifyframework.datastore.generated.model.Event
import com.example.androidkotlin.R
import com.example.androidkotlin.TechoApp
import com.example.androidkotlin.databinding.FragmentEventDetailBinding
import com.example.androidkotlin.databinding.ItemCustomFixedSizeLayout3Binding
import com.example.androidkotlin.ui.tasks.CreateTaskBottomSheetFragment
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.imaginativeworld.whynotimagecarousel.ImageCarousel
import org.imaginativeworld.whynotimagecarousel.listener.CarouselListener
import org.imaginativeworld.whynotimagecarousel.model.CarouselItem
import org.imaginativeworld.whynotimagecarousel.utils.setImage
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [EventDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EventDetailFragment : Fragment() {

    private lateinit var binding: FragmentEventDetailBinding
    private lateinit var eventId: String

    // Elements of UI that has to be populated with info of the event
    private lateinit var chipGroup: ChipGroup
    private lateinit var textDateEvent: TextView
    private lateinit var textHourEvent: TextView
    private lateinit var textDescriptionEvent: TextView
    private lateinit var imageCarousel: ImageCarousel
    private lateinit var imageScaleType: ImageView.ScaleType
    private lateinit var myToolbar: MaterialToolbar

    private val eventsViewModel: EventsViewModel by viewModels {
        EventsViewModelFactory((requireActivity().application as TechoApp).eventsRepository)
    }

    private fun createCarousel() {
        // Register lifecycle. For activity this will be lifecycle/getLifecycle().
        imageCarousel.registerLifecycle(lifecycle)

        // Add listener for animations and overwrite the images
        imageCarousel.carouselListener = object : CarouselListener {
            override fun onCreateViewHolder(
                layoutInflater: LayoutInflater,
                parent: ViewGroup
            ): ViewBinding? {
                return ItemCustomFixedSizeLayout3Binding.inflate(
                    layoutInflater,
                    parent,
                    false
                )
            }

            override fun onBindViewHolder(
                binding: ViewBinding,
                item: CarouselItem,
                position: Int
            ) {
                val currentBinding = binding as ItemCustomFixedSizeLayout3Binding

                currentBinding.imageView.apply {
                    scaleType = imageScaleType

                    setImage(item, R.drawable.ic_wb_cloudy_with_padding)
                }
            }
        }
        // Generate the list of images
        val list = mutableListOf<CarouselItem>()
        for (item in MockDataSet.one) {
            list.add(
                CarouselItem(
                    imageUrl = item
                )
            )
        }
        // Image URL with caption
        list.add(
            CarouselItem(
                imageUrl = "https://techo-app-media-s3dev-dev.s3.amazonaws.com/images/evento1.jpg",
                caption = "Photo in techo"
            )
        )
        imageCarousel.setData(list)
    }

    /**
     * Adds a chip to the chipGroup
     */
    private fun addChip(text: String) {
        val chip: Chip = Chip(requireContext())
        chip.text = text
        chip.setChipStrokeColorResource(R.color.azul_techo)
        chip.setChipStrokeWidthResource(R.dimen.chip_stroke_width)
        chipGroup.addView(chip)
    }

    private fun getEventData(idEvent: String) {
        this.lifecycleScope.launch(Dispatchers.Main) { eventsViewModel.getEvent(idEvent) }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEventDetailBinding.inflate(layoutInflater, container, false);
        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // set the references for the view
        chipGroup = binding.chipGroupTags
        textDateEvent = binding.txtDateEvent
        textHourEvent = binding.txtHourEvent
        textDescriptionEvent = binding.txtEventDescription
        imageCarousel = binding.carouselEvent
        imageScaleType = ImageView.ScaleType.CENTER_CROP
        myToolbar= binding.topAppBarEvent

        // Set the up button
        myToolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
        myToolbar.setNavigationOnClickListener { view ->
            // Navigate to home view
            view.findNavController().navigate(EventDetailFragmentDirections.actionEventDetailFragmentToHomeFragment())
        }

        //create the carousel component
        createCarousel()

        // set the dialog for register with qrCode
        binding.buttonRegisterAttendance.setOnClickListener {
            val qrEventBottomSheetFragment = QrEventBottomSheetFragment()
            qrEventBottomSheetFragment.setEventId(eventId)
            qrEventBottomSheetFragment.show(requireActivity().supportFragmentManager, qrEventBottomSheetFragment.tag)
        }

        // Populate the event with an observer pattern according MVVM
        eventId = EventDetailFragmentArgs.fromBundle(requireArguments()).eventId
        getEventData(eventId)

        eventsViewModel.event.observe(viewLifecycleOwner, Observer {
            val eventResult = it ?: return@Observer
            if (eventResult.success != null) {
                binding.eventModel = eventResult.success as Event

                // Handle logic to populate the event optional info inside the UI
                // Logic for tags
                eventResult.success.tags?.let { list: List<String> ->
                    list.forEach { str: String -> addChip(str) }
                } ?: run {
                    addChip("Ejemplo")
                }

                // Logic to set the date for the event
                val formatterDate = DateTimeFormatter.ofPattern("yyyy-MM-dd")
                val formatterHourDate = DateTimeFormatter.ofPattern("HH:mm:ss")
                var dateYear: LocalDateTime = LocalDateTime.parse(
                    "1986-04-08 12:30",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
                )
                eventResult.success.endDateTime?.let { date: Temporal.DateTime ->
                    dateYear =
                        date.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()
                } ?: run {
                    dateYear =
                        Calendar.getInstance().time.toInstant().atZone(ZoneId.systemDefault())
                            .toLocalDateTime()
                }
                val dateEvent = dateYear.format(formatterDate)
                val hourEvent = dateYear.format(formatterHourDate)
                textDateEvent.setText(dateEvent.toString());
                textHourEvent.setText(hourEvent.toString())

                // Logic to set the description
                eventResult.success.description?.let { description: String ->
                    textDescriptionEvent.setText(description)
                } ?: run {
                    textDescriptionEvent.setText("Descripcion de ejemplo")
                }
            } else {
                Log.e("error", "error")
            }
        } )
    }
}