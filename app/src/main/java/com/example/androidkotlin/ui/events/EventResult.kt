package com.example.androidkotlin.ui.events

import com.amplifyframework.datastore.generated.model.Event

data class EventResult (
    val success: Event? = null,
    val error: String? = null
)