package com.example.androidkotlin.ui.events

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.amplifyframework.datastore.generated.model.Event
import com.example.androidkotlin.NavGraphActivity
import com.example.androidkotlin.adapters.EventsAdapter
import com.example.androidkotlin.databinding.FragmentEventsBinding
import kotlin.collections.ArrayList


class EventsFragment() : Fragment() {

    private lateinit var fragmentHomeBinding : FragmentEventsBinding
    private var eventsAdapter : EventsAdapter? = null
    private lateinit var events : ArrayList<Event>
    private lateinit var eventsViewModel: EventsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentHomeBinding = FragmentEventsBinding.inflate(inflater, container , false)
        eventsViewModel=(activity as NavGraphActivity).eventsViewModel
        eventsViewModel.events.observe(viewLifecycleOwner, Observer{
            val eventsResult = it?: return@Observer
            if ( eventsResult.success != null ){
                events = eventsResult.success as ArrayList<Event>
                fragmentHomeBinding.recyclerViewContact.apply {
                    layoutManager = LinearLayoutManager(context)
                    setHasFixedSize(true)
                    eventsAdapter = EventsAdapter(events)
                    adapter = eventsAdapter
                }
            } else {
                Log.e("error","error")
            }
        } )

        eventsViewModel.loading.observe(viewLifecycleOwner, Observer{
            val isLoading = it?: return@Observer
            if (isLoading) {
                fragmentHomeBinding.loadingItem.visibility = View.VISIBLE
            } else {
                fragmentHomeBinding.loadingItem.visibility = View.GONE
            }
        })

        eventsViewModel.getEvents()
        return fragmentHomeBinding.root
    }

}