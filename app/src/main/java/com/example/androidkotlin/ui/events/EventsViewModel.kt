package com.example.androidkotlin.ui.events
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidkotlin.data.Result
import com.example.androidkotlin.data.events.EventsRepository
import kotlinx.coroutines.*

class EventsViewModel(private val eventsRepository : EventsRepository) : ViewModel() {

    private val _events = MutableLiveData<EventsResult>()
    val events: LiveData<EventsResult> = _events

    private val _event = MutableLiveData<EventResult>()
    val event: LiveData<EventResult> = _event

    private val _attendance = MutableLiveData<AttendanceResult>()
    val attendance: LiveData<AttendanceResult> = _attendance

    private var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        _events.value = EventsResult(error = throwable.message)
    }
    val loading = MutableLiveData<Boolean>()

    fun getEvents(){
        job = CoroutineScope(Dispatchers.IO+ exceptionHandler).launch {
            val result = eventsRepository.getEvents()
            withContext(Dispatchers.Main) {
                if( result is Result.Success ){
                    _events.value = EventsResult(success = result.data)
                } else if ( result is Result.Error ){
                    _events.value = EventsResult(error = result.exception.message)
                }
                loading.value = false
            }
        }
    }

    suspend fun getEvent(id:String){
        viewModelScope.launch(Dispatchers.Main) {
            val result = eventsRepository.getEvent(id)
            if (result is Result.Success) {
                _event.value=EventResult(success = result.data)
            } else if (result is Result.Error) {
                _event.value=EventResult(error = result.exception.message)
            }
        }
    }

    suspend fun takeAttendance(pLat: String, pLong: String, qrCode: String, userId:String){
        viewModelScope.launch(Dispatchers.IO) {
            val result = eventsRepository.takeAttendance(pLat,pLong,qrCode,userId)
            withContext(Dispatchers.Main){
                if (result is Result.Success) {
                    _attendance.value=AttendanceResult(success = result.data)
                    Log.i("POSTDEBUG",result.data)
                } else if (result is Result.Error) {
                    _attendance.value= AttendanceResult(error = result.exception.message)
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}