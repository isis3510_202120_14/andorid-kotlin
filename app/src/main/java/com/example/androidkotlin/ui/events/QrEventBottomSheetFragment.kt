package com.example.androidkotlin.ui.events

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.pm.PackageManager
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.budiyev.android.codescanner.ScanMode
import com.example.androidkotlin.databinding.FragmentQrCodeEventBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_home.container
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.androidkotlin.TechoApp
import com.example.androidkotlin.ui.profile.ProfileViewModel
import com.example.androidkotlin.ui.profile.ProfileViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch


private const val CAMERA_REQUEST_CODE = 101
private const val LOCATION_REQUEST_CODE = 102

class QrEventBottomSheetFragment : BottomSheetDialogFragment() {
    // Variables for the dialog
    private lateinit var binding: FragmentQrCodeEventBinding
    private lateinit var eventId: String
    private lateinit var latitude: String
    private lateinit var longitude: String
    private var boolOnce: Boolean = true
    private lateinit var scanMessage: String
    private val eventsViewModel: EventsViewModel by viewModels {
        EventsViewModelFactory((requireActivity().application as TechoApp).eventsRepository)
    }

    // Variables for the functionality
    private lateinit var codeScanner: CodeScanner
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    fun setEventId(pEventId: String) {
        eventId = pEventId
    }


    private fun handleTakeAttendance(pLat: String, pLong: String, qrCode: String) {
        if (boolOnce) {
            val inProcess: String =
                "Tomando la asistencia..."
            runOnUiThread {
                binding.textQr.text = inProcess
            }
            val userId: String? =
                (requireActivity().application as TechoApp).loginRepository.user?.id
            if (userId != null) {
                this.lifecycleScope.launch(Dispatchers.IO) { eventsViewModel.takeAttendance(pLat,pLong,qrCode,userId) }

            } else {
                throw error("No esta logueado")
            }
            boolOnce = false
        }

    }

    @SuppressLint("MissingPermission")
    private fun codeScanner() {
        codeScanner = CodeScanner(requireActivity(), binding.scannerView)
        codeScanner.apply {
            camera = CodeScanner.CAMERA_BACK
            formats = CodeScanner.TWO_DIMENSIONAL_FORMATS

            autoFocusMode = AutoFocusMode.SAFE
            scanMode = ScanMode.CONTINUOUS
            isAutoFocusEnabled = true
            isFlashEnabled = false

            decodeCallback = DecodeCallback {
                scanMessage = if (it.text.startsWith("TECHO_APP_ATTDCE_E") &&
                    it.text.endsWith(eventId)
                ) {
                    "QR del evento escaneado";
                } else {
                    "Código QR no válido";
                }

                if (scanMessage.equals("QR del evento escaneado")) {
                    val task = fusedLocationProviderClient.lastLocation;
                    task.addOnSuccessListener {
                        latitude = "${it.latitude}"
                        longitude = "${it.longitude}"
                        handleTakeAttendance(latitude, longitude, scanMessage)
                    }
                }
                if (boolOnce) {
                    runOnUiThread {
                        binding.textQr.text = scanMessage
                    }
                }
            }

            errorCallback = ErrorCallback {
                runOnUiThread {
                    Log.e("BottomSheetDialogFragment", "Camera initialization error: ${it.message}")
                }
            }
        }
        binding.scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }


    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        binding = FragmentQrCodeEventBinding.inflate(dialog.layoutInflater, dialog.container, false)
        dialog.setContentView(binding.root)
        binding.cancelQr.setOnClickListener {
            dialog.dismiss()
        }
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())

        // code scanner config
        setupPermissions()
        codeScanner()
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        super.onPause()
        codeScanner.releaseResources()
    }

    override fun onDestroy() {
        super.onDestroy()
        codeScanner.releaseResources()
    }

    private fun makeRequestCamera() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(android.Manifest.permission.CAMERA),
            CAMERA_REQUEST_CODE
        )
    }

    private fun makeRequestLocation() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            LOCATION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                        requireActivity(),
                        "Se necesita el permiso de la camara para poder tomar la asistencia",
                        Toast.LENGTH_SHORT
                    )
                } else {
                    // successful
                }
            }
            LOCATION_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                        requireActivity(),
                        "Se necesita el permiso de la ubicacion para poder tomar la asistencia",
                        Toast.LENGTH_SHORT
                    )
                } else {
                    // successful
                }
            }

        }
    }

    private fun setupPermissions() {
        val permissionCamera = ContextCompat.checkSelfPermission(
            requireActivity(),
            android.Manifest.permission.CAMERA
        )
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            makeRequestCamera()
        }
        val permissionFineLocation = ContextCompat.checkSelfPermission(
            requireActivity(),
            android.Manifest.permission.ACCESS_FINE_LOCATION
        )
        val permissionCoarseLocation = ContextCompat.checkSelfPermission(
            requireActivity(),
            android.Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (permissionFineLocation != PackageManager.PERMISSION_GRANTED && permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            makeRequestLocation()
        }
    }
}