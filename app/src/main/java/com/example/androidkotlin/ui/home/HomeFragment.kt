package com.example.androidkotlin.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.androidkotlin.NavGraphActivity
import com.example.androidkotlin.R
import com.example.androidkotlin.TechoApp
import com.example.androidkotlin.data.PreferencesRepository
import com.example.androidkotlin.databinding.FragmentHomeBinding
import com.example.androidkotlin.ui.courses.CoursesFragment
import com.example.androidkotlin.ui.events.EventsFragment
import com.example.androidkotlin.ui.events.EventsViewModel
import com.example.androidkotlin.ui.events.EventsViewModelFactory
import com.example.androidkotlin.ui.login.LoginViewModel
import com.example.androidkotlin.ui.login.LoginViewModelFactory
import com.example.androidkotlin.ui.profile.ProfileFragment
import com.example.androidkotlin.ui.tasks.TasksFragment

/**
 * A simple [Fragment] subclass.
 * Use the [Home.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {

    private  var fragment: Fragment? = null
    private lateinit var binding : FragmentHomeBinding
    private lateinit var sharedPreferences : PreferencesRepository
    private val loginViewModel: LoginViewModel by viewModels {
        LoginViewModelFactory((requireActivity().application as TechoApp).loginRepository)
    }
    private val eventsViewModel: EventsViewModel by viewModels {
        EventsViewModelFactory((requireActivity().application as TechoApp).eventsRepository)
    }
    private val loginRepository by lazy { (requireActivity().application as TechoApp).loginRepository }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(loginRepository != null && !loginRepository!!.isLoggedIn){
            binding.root.findNavController().navigate(R.id.action_global_loginFragment)
        }
        sharedPreferences = PreferencesRepository(requireActivity().applicationContext)

        val bottomNavigation = binding.chipNavigationBar
        val topBar = binding.topAppBar

        if (savedInstanceState == null){
            childFragmentManager.beginTransaction().replace(R.id.container, EventsFragment()).commit()
            bottomNavigation.setItemSelected(R.id.home)
        }
        bottomNavigation.setOnItemSelectedListener { id ->
            when(id){
                R.id.home -> {
                    fragment =  EventsFragment()
                }
                R.id.tasks ->{
                    fragment = TasksFragment()
                }
                R.id.courses -> {
                    fragment = CoursesFragment()
                }
                R.id.profile -> {
                    fragment = ProfileFragment()
                }
            }
            fragment!!.let {
                childFragmentManager.beginTransaction().replace(R.id.container, it).commit()
            }
        }

        topBar.setOnMenuItemClickListener{
            when(it.itemId){
                R.id.logout -> {
                    loginViewModel.logout()
                    binding.root.findNavController().navigate(HomeFragmentDirections.actionGlobalLoginFragment())
                }
                R.id.mode -> {
                    (requireActivity() as NavGraphActivity).changeTheme()
                }
            }
            true
        }
    }

}