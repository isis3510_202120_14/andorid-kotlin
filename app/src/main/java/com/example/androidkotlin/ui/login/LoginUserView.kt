package com.example.androidkotlin.ui.login

import java.util.*

/**
 * User details post authentication that is exposed to the UI
 */
data class LoginUserView(
    val email: String,
    val fullname: String?,
    val document: String?,
    val phone: String?,
    val birthday: Date?,
    val city: String?
)