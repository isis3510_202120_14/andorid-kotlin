package com.example.androidkotlin.ui.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.viewModelScope
import com.amplifyframework.datastore.generated.model.User
import com.example.androidkotlin.data.login.LoginRepository
import com.example.androidkotlin.data.Result
import com.example.androidkotlin.R
import com.example.androidkotlin.ui.login.forms.ConfirmationFormState
import com.example.androidkotlin.ui.login.forms.SignUpFormState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.Period
import java.time.ZoneId
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    enum class PasswordErrors {
        LOWER, UPPER, DIGIT, SPECIAL, SIZE, NONE
    }

    private val _singUpForm = MutableLiveData<SignUpFormState>()
    val signUpFormState: LiveData<SignUpFormState> = _singUpForm

    private val _confirmationForm = MutableLiveData<ConfirmationFormState>()
    val confirmationForm: LiveData<ConfirmationFormState> = _confirmationForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    fun login(username: String, password: String) {
        viewModelScope.launch(Dispatchers.Main) {
            // can be launched in a separate asynchronous job
            val result = loginRepository.login(username.trim(), password)

            if (result is Result.Success) {
                _loginResult.value = LoginResult(success = getUserView(result))
            } else if (result is Result.Error){
                _loginResult.value = LoginResult(error = translateError(result.exception.message))
            }
        }
    }

    fun signUp(username: String, fullname: String, document: String, phone: String, birthday: Date, city: String, password: String) {
        viewModelScope.launch(Dispatchers.Main) {
            // can be launched in a separate asynchronous job
            val result = loginRepository.signUp(username.trim(), fullname, document.trim(), phone.trim(), birthday, city, password)
            if (result is Result.Success) {
                _loginResult.value =
                    LoginResult(success = getUserView(result))
            } else if (result is Result.Error){
                _loginResult.value = LoginResult(error = translateError(result.exception.message))
            }
        }
    }

    // Confirmation
    fun confirmSignUp(username: String, password: String, confirmationCode: String) {
        viewModelScope.launch(Dispatchers.Main) {
            val result = loginRepository.confirmSignUp(username.trim(), password, confirmationCode)
            if (result is Result.Success) {
                _loginResult.value = LoginResult(success = getUserView(result))
            } else if (result is Result.Error){
                _loginResult.value = LoginResult(error = translateError(result.exception.message))
            }
        }
    }

    fun logout() {
        loginRepository.logout()
    }

    fun signUpDataChanged(username: String, fullname: String, document: String, phone: String, birthday: Date, city: String, password: String, confirmPassword: String) {
        if (!isUserNameValid(username)) {
            _singUpForm.value = SignUpFormState(usernameError = R.string.invalid_username)
        } else if (fullname.isBlank()){
            _singUpForm.value = SignUpFormState(fullnameError = R.string.invalid_fullname)
        } else if (isDocumentValid(document)){
            _singUpForm.value = SignUpFormState(documentError = R.string.invalid_document)
        } else if (phone.isBlank()){
            _singUpForm.value = SignUpFormState(phoneError = R.string.invalid_phone)
        } else if (!isBirthdayValid(birthday)){
            _singUpForm.value = SignUpFormState(birthdayError = R.string.invalid_birthday)
        } else if (city.isBlank()){
            _singUpForm.value = SignUpFormState(cityError = R.string.invalid_city)
        } else if (isPasswordValid(password) != PasswordErrors.NONE) {
            when(isPasswordValid(password)){
                PasswordErrors.LOWER -> _singUpForm.value = SignUpFormState(passwordError = R.string.invalid_password_lower)
                PasswordErrors.UPPER -> _singUpForm.value = SignUpFormState(passwordError = R.string.invalid_password_upper)
                PasswordErrors.SPECIAL -> _singUpForm.value = SignUpFormState(passwordError = R.string.invalid_password_special)
                PasswordErrors.DIGIT -> _singUpForm.value = SignUpFormState(passwordError = R.string.invalid_password_digit)
                PasswordErrors.SIZE -> _singUpForm.value = SignUpFormState(passwordError = R.string.invalid_password_size)
            }
        } else if (!isConfirmPasswordValid(password, confirmPassword)){
            _singUpForm.value = SignUpFormState(confirmPasswordError = R.string.invalid_confirmPassword)
        } else {
            _singUpForm.value = SignUpFormState(isDataValid = true)
        }
    }

    fun confirmationCodeChanged(confirmationCode: String){
        if(!isConfirmationCodeValid(confirmationCode)){
            _confirmationForm.value = ConfirmationFormState(confirmationCodeError = R.string.invalid_confirmationCode)
        } else{
            _confirmationForm.value = ConfirmationFormState(isDataValid = true)
        }
    }

    fun reset(){
        _loginResult.value = LoginResult()
    }

    private fun getUserView(result: Result.Success<User>): LoginUserView{

        return LoginUserView(
            email = result.data.email,
            fullname = result.data.name,
            document = result.data.dni,
            phone = result.data.phone,
            birthday = if( result.data.birthday != null ) result.data.birthday.toDate() else null ,
            city = result.data.city)
        }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username.filter {!it.isWhitespace()}).matches()
        } else {
            username.isNotBlank()
        }
    }

    private fun isDocumentValid(document: String): Boolean {
        val documentPattern: Pattern = Pattern.compile("[a-zA-Z]")
        return documentPattern.matcher(document).find() && document.length >= 9
    }

    private fun isBirthdayValid(birthday: Date): Boolean{
        return Period.between(
            birthday.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate(),
            LocalDate.now()
        ).years > 18
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): PasswordErrors {

        val lower: Pattern = Pattern.compile("[a-z]")
        val upper: Pattern = Pattern.compile("[A-Z]")
        val digit: Pattern = Pattern.compile("[0-9]")
        val special: Pattern = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]")

        val hasLower: Matcher = lower.matcher(password)
        val hasUpper: Matcher = upper.matcher(password)
        val hasDigit: Matcher = digit.matcher(password)
        val hasSpecial: Matcher = special.matcher(password)

        if (!hasLower.find()){
            return PasswordErrors.LOWER
        }
        if (!hasUpper.find()){
            return PasswordErrors.UPPER
        }
        if (!hasDigit.find()){
            return PasswordErrors.DIGIT
        }
        if (!hasSpecial.find()){
            return PasswordErrors.SPECIAL
        }
        if (password.length < 8){
            return PasswordErrors.SIZE
        }
        return PasswordErrors.NONE

    }

    private fun isConfirmPasswordValid(password: String, confirmPassword: String): Boolean{
        return password == confirmPassword
    }

    private fun isConfirmationCodeValid(confirmationCode: String): Boolean{
        return confirmationCode.length == 6
    }

    // Utils
    private fun translateError(error: String?): Int{
        if (error != null){
            Log.i("error", error)
            return with(error){
                when{
                    contains("User not found in the system") -> R.string.error_user_doesnt_exist
                    contains("One or more parameters are incorrect") -> R.string.error_user_incorrect
                    contains("Username already exists in the system") -> R.string.error_user_already_exists
                    contains("Failed since user is not authorized") -> R.string.error_password_incorrect
                    contains("Required to reset the password of the user") -> R.string.error_reset_password_required
                    //contains("Sign in not complete") ->  R.string.error_signin_not_complete
                    contains("User not confirmed in the system") ->  R.string.error_signin_not_complete
                    contains("Confirmation code entered is not correct") ->  R.string.error_confirmation_code_incorrect
                    contains("Sign in failed") ->  R.string.error_sign_failed
                    else -> R.string.error_generic
                }
            }
        }
        return R.string.error_generic
    }

}