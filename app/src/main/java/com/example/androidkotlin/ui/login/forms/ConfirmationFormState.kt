package com.example.androidkotlin.ui.login.forms

data class ConfirmationFormState(
    val confirmationCodeError: Int? = null,
    val isDataValid: Boolean = false
)