package com.example.androidkotlin.ui.login.forms
/**
 * Data validation state of the login form.
 */
data class SignUpFormState(
    val usernameError: Int? = null,
    val emptyError: Int? = null,
    val fullnameError: Int? = null,
    val documentError: Int? = null,
    val phoneError: Int? = null,
    val birthdayError: Int? = null,
    val cityError: Int? = null,
    val passwordError: Int? = null,
    val confirmPasswordError: Int? = null,
    val isDataValid: Boolean = false
)