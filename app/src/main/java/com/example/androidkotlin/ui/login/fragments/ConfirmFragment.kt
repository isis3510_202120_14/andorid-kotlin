package com.example.androidkotlin.ui.login.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.androidkotlin.NavGraphActivity
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentConfirmBinding
import com.example.androidkotlin.ui.login.LoginUserView
import com.example.androidkotlin.ui.login.LoginViewModel
import com.example.androidkotlin.utilities.CheckNetwork
import com.example.androidkotlin.utilities.afterTextChanged

class ConfirmFragment : Fragment() {
    private lateinit var binding: FragmentConfirmBinding
    private lateinit var loginViewModel: LoginViewModel

    private val args: ConfirmFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentConfirmBinding.inflate(inflater, container , false)
        loginViewModel = (activity as NavGraphActivity).loginViewModel
        loginViewModel.reset()

        val confirmationCode = binding.txtCode
        val confirmationBtn = binding.confirmBtn
        val returnBtn = binding.txtReturn

        loginViewModel.confirmationForm.observe(viewLifecycleOwner, Observer {
            val confirmationState = it ?: return@Observer

            confirmationBtn.isEnabled = confirmationState.isDataValid

            if (confirmationState.confirmationCodeError != null) {
                confirmationCode.error = getString(confirmationState.confirmationCodeError)
            }
        })

        loginViewModel.loginResult.observe(viewLifecycleOwner, Observer {
            val loginResult = it ?: return@Observer

            if (loginResult.error != null) {
                showLoginFailed(loginResult.error, "Error al confirmar el email")
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
            }

            confirmationBtn.isEnabled=true
        })
        confirmationCode.afterTextChanged {
            loginViewModel.confirmationCodeChanged(
                confirmationCode.text.toString()
            )
        }

        confirmationBtn.setOnClickListener{
            confirmationBtn.isEnabled=false
            if ( CheckNetwork.isInternetAvailable((activity as NavGraphActivity).applicationContext) ){
                loginViewModel.confirmSignUp(args.usernameTxt, args.passwordTxt, confirmationCode.text.toString())
            }else{
                showLoginFailed(R.string.error_check_connection, "Problemas de conexión")
                confirmationBtn.isEnabled=true
            }

        }

        returnBtn.setOnClickListener{
            binding.root.findNavController().navigate(ConfirmFragmentDirections.actionConfirmFragmentToLoginFragment())
        }

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun updateUiWithUser(model: LoginUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.email

        Toast.makeText(
            activity?.applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun showLoginFailed(@StringRes errorString: Int, titulo: String) {
        binding.errorLayout.visibility = View.VISIBLE
        binding.errorTitle.text  = titulo
        binding.errorSubtitle.text = resources.getString(errorString)
    }

}