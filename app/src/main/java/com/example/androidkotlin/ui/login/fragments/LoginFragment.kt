package com.example.androidkotlin.ui.login.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.example.androidkotlin.NavGraphActivity
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentLoginBinding
import com.example.androidkotlin.ui.login.LoginUserView
import com.example.androidkotlin.ui.login.LoginViewModel
import com.example.androidkotlin.utilities.CheckNetwork
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.*




class LoginFragment : Fragment() {

    // State
    private val scope = CoroutineScope(Job() + Dispatchers.IO)

    private lateinit var binding: FragmentLoginBinding
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        loginViewModel = (activity as NavGraphActivity).loginViewModel
        loginViewModel.reset()

        // LOGIN
        val username = binding.txtEmail
        val password = binding.txtPassword
        val login = binding.loginBtn
        val signUp = binding.signUpTxt

        loginViewModel.loginResult.observe(viewLifecycleOwner, Observer {
            val loginResult = it ?: return@Observer
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error,"Error al iniciar sesión")
                if (loginResult.error == R.string.error_signin_not_complete) {
                    binding.root.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToConfirmFragment(username.text.toString(), password.text.toString()))
                }
                else{
                    login.isEnabled = true
                }
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
                binding.root.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
            }
        })

        login.setOnClickListener {
            login.isEnabled=false
            if ( CheckNetwork.isInternetAvailable((activity as NavGraphActivity).applicationContext) ){
                loginViewModel.login(username.text.toString(), password.text.toString())
            }else{
                showLoginFailed(R.string.error_check_connection, "Problemas de conexión")
                login.isEnabled=true
            }
        }

        signUp.setOnClickListener {
            binding.root.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToSignupFragment())
        }

        // Error ui
        binding.errorClose.setOnClickListener{
            binding.errorLayout.visibility = View.INVISIBLE
        }

        return binding.root
    }

    private fun updateUiWithUser(model: LoginUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.email

        Toast.makeText(
            activity?.applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun showLoginFailed(@StringRes errorString: Int, titulo: String) {
        binding.errorLayout.visibility = View.VISIBLE
        binding.errorTitle.text  = titulo
        binding.errorSubtitle.text = resources.getString(errorString)
    }

}