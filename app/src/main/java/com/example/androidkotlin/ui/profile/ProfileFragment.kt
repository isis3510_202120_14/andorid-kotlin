package com.example.androidkotlin.ui.profile

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.amplifyframework.datastore.generated.model.Event
import com.amplifyframework.datastore.generated.model.User
import com.example.androidkotlin.NavGraphActivity
import com.example.androidkotlin.R
import com.example.androidkotlin.adapters.EventsAdapter
import com.example.androidkotlin.databinding.FragmentProfileBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileFragment : Fragment() {

    private lateinit var viewModel: ProfileViewModel
    private lateinit var binding : FragmentProfileBinding
    private lateinit var user : User

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentProfileBinding.inflate(inflater, container, false)
        viewModel = (activity as NavGraphActivity).profileViewModel
        Log.i("PROFILE","Hasta acá vamos bien1")

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getUserInfo()
        Log.i("PUEBA_FR", viewModel.user.toString())

        viewModel.user.observe(viewLifecycleOwner, Observer{
            Log.i("PROFILE","Hasta acá vamos bien")
            val userResult = it?: return@Observer
            if ( userResult.success != null ){
                Log.i("PROFILE","El usuario no era nulo")
                user = userResult.success
                binding.userModel = user
            } else {
                Log.i("PROFILE","El usuario era nulo")
                Log.e("error","error")
            }
        } )
    }

    private fun getUserInfo(){
        this.lifecycleScope.launch(Dispatchers.Main) { viewModel.getUser() }
    }


}