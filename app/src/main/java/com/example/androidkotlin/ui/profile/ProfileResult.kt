package com.example.androidkotlin.ui.profile

import com.amplifyframework.datastore.generated.model.User

data class ProfileResult(
    val success: User? = null,
    val error: String? = null
)