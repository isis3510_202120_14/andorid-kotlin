package com.example.androidkotlin.ui.profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidkotlin.data.Result
import com.example.androidkotlin.data.login.LoginRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ProfileViewModel(private val loginRepository: LoginRepository): ViewModel(){

    private val _user = MutableLiveData<ProfileResult>()
    val user: LiveData<ProfileResult> = _user

    fun getUser(){
        viewModelScope.launch(Dispatchers.Main) {
            val result = loginRepository.getUserProfile()
            if (result is Result.Success) {
                _user.value=ProfileResult(success = result.data)
            } else if (result is Result.Error) {
                _user.value=ProfileResult(error = result.exception.message)
            }
        }
    }

}