package com.example.androidkotlin.ui.tasks

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentCreateTaskBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_home.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class CreateTaskBottomSheetFragment : BottomSheetDialogFragment() {
    private lateinit var binding: FragmentCreateTaskBinding

    private lateinit var tasksFragment: TasksFragment
    private lateinit var alarmManager: AlarmManager
    var timePickerDialog: TimePickerDialog? = null
    var datePickerDialog: DatePickerDialog? = null

    var taskId: String = "0"
    var isEdit = false
    var task: TaskUserView? = null

    var mYear = 0
    var mMonth = 0
    var mDay = 0

    var mHour = 0
    var mMinute = 0

    private val mBottomSheetBehaviorCallback: BottomSheetCallback = object : BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss()
            }
        }
        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    }

    fun setTaskId(
        taskId: String,
        isEdit: Boolean,
        tasksFragment: TasksFragment
    ) {
        this.taskId = taskId
        this.isEdit = isEdit
        this.tasksFragment = tasksFragment
    }

    fun setTaskItem(task: TaskUserView) {
        this.task =  task
    }

    @SuppressLint("RestrictedApi", "ClickableViewAccessibility")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        binding = FragmentCreateTaskBinding.inflate(dialog.layoutInflater, dialog.container, false)
        dialog.setContentView(binding.root)

        alarmManager = requireActivity().getSystemService(Context.ALARM_SERVICE) as AlarmManager
        binding.addTask.setOnClickListener {
            binding.addTask.isEnabled = false
            if (validateFields()) saveTask()
        }

        ArrayAdapter.createFromResource(
            dialog.context,
            R.array.task_type_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            binding.typeSpinner.adapter = adapter
        }

        // Duedate dater
        binding.taskDate.setOnTouchListener { _: View?, motionEvent: MotionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                val c = Calendar.getInstance()
                mYear = c[Calendar.YEAR]
                mMonth = c[Calendar.MONTH]
                mDay = c[Calendar.DAY_OF_MONTH]

                datePickerDialog = DatePickerDialog(requireActivity(),
                    { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                        mYear = year
                        mMonth = monthOfYear
                        mDay = dayOfMonth

                        binding.taskDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                        datePickerDialog!!.dismiss()
                    }, mYear, mMonth, mDay)
                datePickerDialog!!.datePicker.minDate = System.currentTimeMillis() - 1000
                datePickerDialog!!.show()
            }
            true
        }

        // Duedate time
        binding.taskTime.setOnTouchListener { _: View?, motionEvent: MotionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                // Get Current Time
                val c = Calendar.getInstance()
                mHour = c[Calendar.HOUR_OF_DAY]
                mMinute = c[Calendar.MINUTE]

                // Launch Time Picker Dialog
                timePickerDialog = TimePickerDialog(requireActivity(),
                    { _: TimePicker?, hourOfDay: Int, minute: Int ->
                        mHour = hourOfDay
                        mMinute = minute

                        binding.taskTime.setText("$hourOfDay:$minute")
                        timePickerDialog!!.dismiss()
                    }, mHour, mMinute, false)
                timePickerDialog!!.show()
            }
            true
        }
    }

    fun validateFields(): Boolean {
        return if (binding.addTaskTitle.text.toString().equals("", ignoreCase = true)) {
            Toast.makeText(activity, "Please enter a valid title", Toast.LENGTH_SHORT).show()
            false
        } else if (binding.addTaskDescription.text.toString().equals("", ignoreCase = true)) {
            Toast.makeText(activity, "Please enter a valid description", Toast.LENGTH_SHORT).show()
            false
        } else if (binding.taskDate.text.toString().equals("", ignoreCase = true)) {
            Toast.makeText(activity, "Please enter date", Toast.LENGTH_SHORT).show()
            false
        } else if (binding.taskTime.text.toString().equals("", ignoreCase = true)) {
            Toast.makeText(activity, "Please enter time", Toast.LENGTH_SHORT).show()
            false
        } else {
            true
        }
    }

    private fun saveTask() {
        if( isEdit ){
            tasksFragment.updateTask(
                task!!.id,
                TaskUserView(
                    id =  task!!.id,
                    name = binding.addTaskTitle.text.toString(),
                    description = binding.addTaskDescription.text.toString(),
                    type = binding.typeSpinner.selectedItem.toString(),
                    dueDate = LocalDateTime.of(mYear, mMonth, mDay, mHour, mMinute),
                    isCompleted = false)
            )
        }else{
            tasksFragment.createTask(
                TaskUserView(
                    id = "0",
                    name = binding.addTaskTitle.text.toString(),
                    description = binding.addTaskDescription.text.toString(),
                    type = binding.typeSpinner.selectedItem.toString(),
                    dueDate = LocalDateTime.of(mYear, mMonth, mDay, mHour, mMinute),
                    isCompleted = false)
            )
        }
    }

    fun onCreatedTask(){
        try {
            dismiss()
        }
        catch (e: Exception){
            Log.i("TaskDebug", e.message.toString())
        }
    }

    fun setDataInUI(task: TaskUserView) {
        binding.addTaskTitle.setText(task.name)
        binding.addTaskDescription.setText(task.description)
        binding.taskDate.setText(task.dueDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
        binding.taskTime.setText(task.dueDate.format(DateTimeFormatter.ofPattern("HH:mm")))
    }

    companion object {
        var count = 0
    }
}