package com.example.androidkotlin.ui.tasks

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.View
import com.example.androidkotlin.databinding.FragmentCalendarViewBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.util.*
import android.R
import android.util.TypedValue
import com.applandeo.materialcalendarview.EventDay
import kotlinx.android.synthetic.main.fragment_home.*
import java.time.ZoneId

class ShowCalendarViewBottomSheet : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentCalendarViewBinding
    private lateinit var fragment: TasksFragment
    private lateinit var tasks: List<EventDay>

    private val mBottomSheetBehaviorCallback: BottomSheetCallback = object : BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss()
            }
        }
        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        binding = FragmentCalendarViewBinding.inflate(dialog.layoutInflater, dialog.container, false)
        dialog.setContentView(binding.root)

        val value = TypedValue()
        dialog.context.theme.resolveAttribute(R.attr.colorPrimary, value, true)
        binding.calendarView.setHeaderColor( value.data )

        binding.calendarView.setEvents(fragment.tasks.map {
            EventDay(
                GregorianCalendar.from(it.dueDate.atZone(ZoneId.systemDefault()))
            )
        })

        binding.back.setOnClickListener { dialog.dismiss() }
    }

    fun setFragment(fragment: TasksFragment){
        this.fragment = fragment
    }
}
