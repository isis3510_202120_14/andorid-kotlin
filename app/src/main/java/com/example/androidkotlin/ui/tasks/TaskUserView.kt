package com.example.androidkotlin.ui.tasks

import java.time.LocalDateTime

data class TaskUserView(
    val id: String,
    val name: String,
    val description: String,
    val type: String,
    val dueDate: LocalDateTime,
    val isCompleted: Boolean
    )