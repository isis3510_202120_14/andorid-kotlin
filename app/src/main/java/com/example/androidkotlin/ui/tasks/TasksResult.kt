package com.example.androidkotlin.ui.tasks

data class TasksResult (
    val success: List<TaskUserView>? = null,
    val error: String? = null
)