package com.example.androidkotlin.ui.tasks

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.amplifyframework.core.model.temporal.Temporal
import com.amplifyframework.datastore.generated.model.Task
import com.amplifyframework.datastore.generated.model.TaskType
import com.example.androidkotlin.data.Result
import com.example.androidkotlin.data.tasks.TasksRepository
import kotlinx.coroutines.*
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class TasksViewModel(private val tasksRepository : TasksRepository): ViewModel() {

    private val _tasks = MutableLiveData<TasksResult>()
    val tasks: LiveData<TasksResult> = _tasks

    private val _getTask = MutableLiveData<TaskResult>()
    val getTask: LiveData<TaskResult> = _getTask

    private val _createTask = MutableLiveData<TaskResult>()
    val createTask: LiveData<TaskResult> = _createTask

    private val _updateTask = MutableLiveData<TaskResult>()
    val updateTask: LiveData<TaskResult> = _updateTask

    private val _deleteTask = MutableLiveData<TaskResult>()
    val deleteTask: LiveData<TaskResult> = _deleteTask

    private var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        _tasks.value = TasksResult(error = throwable.message)
    }
    val loading = MutableLiveData<Boolean>()

    fun getTasks(){
        job = CoroutineScope(Dispatchers.IO+ exceptionHandler).launch {
            val result = tasksRepository.getTasks()
            withContext(Dispatchers.Main) {
                if( result is Result.Success ){
                    _tasks.value = TasksResult(success = getUserView(result.data) )
                } else if ( result is Result.Error ){
                    _tasks.value = TasksResult(error = result.exception.message)
                }
                loading.value = false
            }
        }
    }

    fun getTask(id:String){
        job = CoroutineScope(Dispatchers.IO+ exceptionHandler).launch {
            val result = tasksRepository.getTask(id)
            withContext(Dispatchers.Main) {
                if( result is Result.Success ){
                    _getTask.value = TaskResult(success = getUserView(result.data) )
                } else if ( result is Result.Error ){
                    _getTask.value = TaskResult(error = result.exception.message)
                }
            }
        }
    }

    fun createTask(task: TaskUserView){
        job = CoroutineScope(Dispatchers.IO+ exceptionHandler).launch {
            val result = tasksRepository.createTask(getTaskModel(task))
            withContext(Dispatchers.Main) {
                if( result is Result.Success ){
                    _createTask.value = TaskResult(success = getUserView(result.data) )
                } else if ( result is Result.Error ){
                    _createTask.value = TaskResult(error = result.exception.message)
                }
                loading.value = false
            }
        }
    }

    fun updateTask(id:String, task: TaskUserView){
        job = CoroutineScope(Dispatchers.IO+ exceptionHandler).launch {
            val result = tasksRepository.updateTask(id, getTaskModel(task))
            withContext(Dispatchers.Main) {
                if( result is Result.Success ){
                    _updateTask.value = TaskResult(success = getUserView(result.data) )
                } else if ( result is Result.Error ){
                    _updateTask.value = TaskResult(error = result.exception.message)
                }
            }
        }
    }

    fun completeTask(id:String, task: TaskUserView){
        job = CoroutineScope(Dispatchers.IO+ exceptionHandler).launch {
            val result = tasksRepository.completeTask(id, getTaskModel(task))
            withContext(Dispatchers.Main) {
                if( result is Result.Success ){
                    _updateTask.value = TaskResult(success = getUserView(result.data) )
                } else if ( result is Result.Error ){
                    _updateTask.value = TaskResult(error = result.exception.message)
                }
            }
        }
    }

    fun deleteTask(id:String){
        job = CoroutineScope(Dispatchers.IO+ exceptionHandler).launch {
            val result = tasksRepository.deleteTask(id)
            withContext(Dispatchers.Main) {
                if( result is Result.Success ){
                    _deleteTask.value = TaskResult(success = getUserView(result.data) )
                } else if ( result is Result.Error ){
                    _deleteTask.value = TaskResult(error = result.exception.message)
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

    private fun getUserView(original: Task): TaskUserView {
        return TaskUserView(
            id = original.id,
            name = original.name,
            description = original.description,
            type = getTaskType(original.taskType),
            dueDate = original.dueDate.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
            isCompleted = original.isCompleted)
    }

    private fun getUserView(original: List<Task>): List<TaskUserView> {
        return original.map {
            getUserView(it)
        }.toList()
    }

    private fun getTaskModel(original: TaskUserView): Task{
        val df = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm'Z'")

        return Task.builder()
            .id(original.id)
            .name(original.name)
            .description(original.description)
            .taskType(getTaskType(original.type))
            .dueDate(Temporal.DateTime(original.dueDate.format(df)))
            .isCompleted(original.isCompleted)
            .build()
    }

    private val CAPACITACION = "Capacitación"
    private val CONSTRUCCION_VIVIENDAS = "Construcción de viviendas"
    private val PLANEACION = "Planeación"
    private val EJECUCION_ACTIVIDAD = "Ejecución de actividad"

    fun getTaskType(type: String): TaskType{
        return when(type){
            CAPACITACION -> TaskType.CAPACITACION
            CONSTRUCCION_VIVIENDAS -> TaskType.CONSTRUCCION_VIVIENDAS
            PLANEACION -> TaskType.PLANEACION
            EJECUCION_ACTIVIDAD -> TaskType.EJECUCION_ACTIVIDAD
            else -> TaskType.PLANEACION
        }
    }

    fun getTaskType(type: TaskType): String{
        return when(type){
            TaskType.CAPACITACION -> CAPACITACION
            TaskType.CONSTRUCCION_VIVIENDAS -> CONSTRUCCION_VIVIENDAS
            TaskType.PLANEACION -> PLANEACION
            TaskType.EJECUCION_ACTIVIDAD -> EJECUCION_ACTIVIDAD
            else -> PLANEACION
        }
    }
}