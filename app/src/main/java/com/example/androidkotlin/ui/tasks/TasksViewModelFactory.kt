package com.example.androidkotlin.ui.tasks

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.androidkotlin.data.tasks.TasksRepository

class TasksViewModelFactory (private val tasksRepository: TasksRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return TasksViewModel(tasksRepository) as T
    }
}